package com.dacodes.venadostest.data.remote.base

data class ApiResponse<T>(val success: Boolean,
                          val data: T)