package com.dacodes.venadostest.data.remote.base

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class RequestInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()
                .header("Accept", "application/json")
                .method(original.method(), original.body())
        return chain.proceed(builder.build())
    }
}
