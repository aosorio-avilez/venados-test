package com.dacodes.venadostest.data.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class Games(val games: List<Game>)

data class Game(
        val local: Boolean,
        val opponent: String,
        @SerializedName("opponent_image")
        val opponentImage: String,
        val datetime: Date,
        val league: String,
        val image: String,
        @SerializedName("home_score")
        val homeScore: Int,
        @SerializedName("away_score")
        val awayScore: Int
) {

    fun getMonthName(): String {
        val calendar = Calendar.getInstance()
        calendar.time = datetime
        return String.format(Locale.getDefault(), "%tB", calendar).toUpperCase()
    }
}