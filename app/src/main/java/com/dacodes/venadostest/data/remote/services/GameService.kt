package com.dacodes.venadostest.data.remote.services

import com.dacodes.venadostest.data.models.Games
import com.dacodes.venadostest.data.remote.base.ApiCallback
import com.dacodes.venadostest.data.remote.base.ApiResponse
import com.dacodes.venadostest.data.remote.base.BaseService
import retrofit2.Call
import retrofit2.http.GET

class GameService : BaseService() {

    interface GamesEndpoint {
        @GET("games")
        fun games(): Call<ApiResponse<Games>>
    }

    fun games(callback: (Games) -> Unit, errorCallback: (Int, Throwable) -> Unit) {
        val endpoint = getEndpoint(GamesEndpoint::class.java)
        endpoint.games().enqueue(ApiCallback { status, data, error ->
            if (error != null) {
                errorCallback(status, error)
                return@ApiCallback
            }

            data?.let { callback(it.data) }
        })
    }
}