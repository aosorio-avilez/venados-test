package com.dacodes.venadostest.data.models

import com.google.gson.annotations.SerializedName

data class Team(@SerializedName("team") val players: Players)

data class Players(val forwards: List<Player>,
                   val centers: List<Player>,
                   val defenses: List<Player>,
                   val goalkeepers: List<Player>,
                   val coaches: List<Coach>)