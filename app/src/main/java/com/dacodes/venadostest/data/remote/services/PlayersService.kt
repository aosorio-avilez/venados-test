package com.dacodes.venadostest.data.remote.services

import com.dacodes.venadostest.data.models.Players
import com.dacodes.venadostest.data.models.Team
import com.dacodes.venadostest.data.remote.base.ApiCallback
import com.dacodes.venadostest.data.remote.base.ApiResponse
import com.dacodes.venadostest.data.remote.base.BaseService
import retrofit2.Call
import retrofit2.http.GET

class PlayersService : BaseService() {
    interface PlayersEndpoint {
        @GET("players")
        fun players(): Call<ApiResponse<Team>>
    }

    fun players(callback: (Players) -> Unit, errorCallback: (Int, Throwable) -> Unit) {
        val endpoint = getEndpoint(PlayersEndpoint::class.java)
        endpoint.players().enqueue(ApiCallback { status, data, error ->
            if (error != null) {
                errorCallback(status, error)
                return@ApiCallback
            }

            data?.let { callback(it.data.players) }
        })
    }
}