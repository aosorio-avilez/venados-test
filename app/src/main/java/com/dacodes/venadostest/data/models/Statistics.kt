package com.dacodes.venadostest.data.models

import com.google.gson.annotations.SerializedName

data class Statistics(val statistics: List<Statistic>)

data class Statistic(
        val position: Int,
        val image: String,
        val team: String,
        val games: Int,
        val win: Int,
        val loss: Int,
        val tie: Int,
        @SerializedName("f_goals")
        val fGoals: Int,
        @SerializedName("a_goals")
        val aGoals: Int,
        @SerializedName("score_diff")
        val scoreDiff: Int,
        val points: Int,
        @SerializedName("efec")
        val effectiveness: Int
)