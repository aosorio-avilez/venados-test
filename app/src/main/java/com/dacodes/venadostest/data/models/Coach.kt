package com.dacodes.venadostest.data.models

import java.util.*

data class Coach(val name: String,
                 val firstSurname: String,
                 val secondSurname: String,
                 val birthday: Date,
                 val birthPlace: String,
                 val weight: Double,
                 val height: Double,
                 val role: String,
                 val roleShort: String,
                 val image: String)