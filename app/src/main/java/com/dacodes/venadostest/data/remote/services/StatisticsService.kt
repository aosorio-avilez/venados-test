package com.dacodes.venadostest.data.remote.services

import com.dacodes.venadostest.data.models.Statistics
import com.dacodes.venadostest.data.remote.base.ApiCallback
import com.dacodes.venadostest.data.remote.base.ApiResponse
import com.dacodes.venadostest.data.remote.base.BaseService
import retrofit2.Call
import retrofit2.http.GET

class StatisticsService: BaseService() {
    interface StatisticsEndpoint {
        @GET("statistics")
        fun statistics(): Call<ApiResponse<Statistics>>
    }

    fun statistics(callback: (Statistics) -> Unit, errorCallback: (Int, Throwable) -> Unit) {
        val endpoint = getEndpoint(StatisticsEndpoint::class.java)
        endpoint.statistics().enqueue(ApiCallback { status, data, error ->
            if (error != null) {
                errorCallback(status, error)
                return@ApiCallback
            }

            data?.let { callback(it.data) }
        })
    }
}