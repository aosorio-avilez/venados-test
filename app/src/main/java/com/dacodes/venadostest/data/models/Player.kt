package com.dacodes.venadostest.data.models

import com.google.gson.annotations.SerializedName
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

data class Player(val name: String,
                  @SerializedName("first_surname")
                  val firstSurname: String,
                  @SerializedName("second_surname")
                  val secondSurname: String,
                  val birthday: Date,
                  @SerializedName("birth_place")
                  val birthPlace: String,
                  val weight: Double,
                  val height: Double,
                  val position: String,
                  val number: Int,
                  @SerializedName("position_short")
                  val positionShort: String,
                  @SerializedName("last_team")
                  val lastTeam: String,
                  val image: String) {

    fun getFullName(): String = String.format("%s %s %s", name, firstSurname, secondSurname)

    fun getBirthdate(): String {
        val datetimeFormat: DateFormat = SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault())
        return datetimeFormat.format(birthday)
    }

    fun getWeight() = String.format("%s KG", weight)

    fun getHeigh() = String.format("%s M", height)
}