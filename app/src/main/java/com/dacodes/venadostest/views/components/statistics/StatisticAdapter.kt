package com.dacodes.venadostest.views.components.statistics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Statistic
import kotlinx.android.synthetic.main.item_statistic.view.*

class StatisticAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var statistics: List<Statistic> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return StatisticsViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_statistic, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as StatisticsViewHolder).bind(statistics[position])
    }

    override fun getItemCount(): Int {
        return statistics.size
    }

    fun setstatistics(statistics: List<Statistic>) {
        this.statistics = statistics
        notifyDataSetChanged()
    }

    internal inner class StatisticsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Statistic) = with(itemView) {
            positionLabel?.text = item.position.toString()
            nameLabel?.text = item.team
            gamesPlayedLabel?.text = item.games.toString()
            goalsDifferenceLabel?.text = item.scoreDiff.toString()
            pointsLabel?.text = item.points.toString()
            Glide.with(context)
                    .load(item.image)
                    .into(logoImageView)
        }
    }
}