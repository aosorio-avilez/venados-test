package com.dacodes.venadostest.views.components.statistics

import com.dacodes.venadostest.data.remote.services.StatisticsService

class StatisticsPresenter(private var view: StatisticsContract.View?,
                          private var statisticsService: StatisticsService?) : StatisticsContract.Presenter {

    override fun requestStatistics() {
        view?.showProgress()

        statisticsService?.statistics({
            view?.hideProgress()
            view?.showStatistics(it.statistics)
        }, { _, _ ->
            view?.hideProgress()
        })
    }

    override fun bind() {
        view?.setupView()
        requestStatistics()
    }

    override fun unbind() {
        statisticsService = null
        view = null
    }
}