package com.dacodes.venadostest.views.components.players

import com.dacodes.venadostest.data.models.Player
import com.dacodes.venadostest.views.base.BasePresenter
import com.dacodes.venadostest.views.base.BaseView
import com.dacodes.venadostest.views.base.Progressable

interface PlayersContract {

    interface View : BaseView<Presenter>, Progressable {

        fun setupView()

        fun showPlayers(players: List<Player>)

        fun showPlayerDetailDialog(player: Player)
    }

    interface Presenter : BasePresenter {

        fun requestPlayers()

        fun onPlayerSelected(player: Player)
    }
}