package com.dacodes.venadostest.views.components.main

import androidx.fragment.app.Fragment
import com.dacodes.venadostest.R
import com.dacodes.venadostest.views.components.games.GamesFragment
import com.dacodes.venadostest.views.components.players.PlayersFragment
import com.dacodes.venadostest.views.components.statistics.StatisticsFragment

class MainPresenter(private var view: MainContract.View?) : MainContract.Presenter {

    override fun onMenuOptionSelected(menuId: Int) {
        val fragment: Fragment = when (menuId) {
            R.id.nav_home -> GamesFragment()
            R.id.nav_statistics -> StatisticsFragment()
            R.id.nav_players -> PlayersFragment()
            else -> GamesFragment()
        }
        view?.showFragment(fragment)
    }

    override fun bind() {
        view?.setupView()
    }

    override fun unbind() {
        view = null
    }
}