package com.dacodes.venadostest.views.components.players

import com.dacodes.venadostest.data.models.Player
import com.dacodes.venadostest.data.remote.services.PlayersService

class PlayersPresenter(private var view: PlayersContract.View?,
                       private var service: PlayersService?) : PlayersContract.Presenter {

    override fun requestPlayers() {
        view?.showProgress()

        service?.players({
            view?.hideProgress()
            val players = it.forwards.union(it.centers).union(it.defenses).union(it.goalkeepers).toList()
            view?.showPlayers(players)
        }, { _, _ ->
            view?.hideProgress()
        })
    }

    override fun onPlayerSelected(player: Player) {
        view?.showPlayerDetailDialog(player)
    }

    override fun bind() {
        view?.setupView()
        requestPlayers()
    }

    override fun unbind() {
        service = null
        view = null
    }
}