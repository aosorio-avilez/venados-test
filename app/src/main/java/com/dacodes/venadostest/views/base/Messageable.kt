package com.dacodes.venadostest.views.base

interface Messageable {
    fun showMessage(message: String)

    fun showDialog(title: String, message: String)
}