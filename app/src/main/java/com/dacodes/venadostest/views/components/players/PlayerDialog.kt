package com.dacodes.venadostest.views.components.players

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Player

class PlayerDialog @SuppressLint("InflateParams") constructor(context: Context, player: Player) : AlertDialog(context) {

    private var customView: View? = null

    init {
        this.customView = LayoutInflater.from(context).inflate(R.layout.dialog_player_detail, null)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setView(customView)
        setCancelable(true)
        setPlayer(player)
    }

    private fun setPlayer(player: Player) {
        customView?.findViewById<ImageView>(R.id.pictureImageView)?.apply {
            Glide.with(context)
                    .load(player.image)
                    .apply(RequestOptions.circleCropTransform())
                    .into(this)
        }
        customView?.findViewById<TextView>(R.id.nameLabel)?.apply { text = player.getFullName() }
        customView?.findViewById<TextView>(R.id.positionLabel)?.apply { text = player.position }
        customView?.findViewById<TextView>(R.id.birthdateLabel)?.apply { text = player.getBirthdate() }
        customView?.findViewById<TextView>(R.id.birthPlaceLabel)?.apply { text = player.birthPlace }
        customView?.findViewById<TextView>(R.id.weightLabel)?.apply { text = player.getWeight() }
        customView?.findViewById<TextView>(R.id.heightLabel)?.apply { text = player.getHeigh() }
        customView?.findViewById<TextView>(R.id.lastTeamLabel)?.apply { text = player.lastTeam }
    }
}