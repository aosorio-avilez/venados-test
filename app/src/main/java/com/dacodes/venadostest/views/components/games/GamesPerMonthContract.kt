package com.dacodes.venadostest.views.components.games

import com.dacodes.venadostest.data.models.Game
import com.dacodes.venadostest.views.base.BasePresenter
import com.dacodes.venadostest.views.base.BaseView
import com.dacodes.venadostest.views.base.Progressable

interface GamesPerMonthContract {

    interface View : BaseView<Presenter>, Progressable {

        fun setupView()

        fun showGames(gamesByMonth: Map<String, List<Game>>)

        fun scheduleGame(game: Game)

        fun showEmptyView(show: Boolean = true)
    }

    interface Presenter : BasePresenter {

        fun requestGames(withRefreshing: Boolean = false)

        fun setGames(games: List<Game>)

        fun onScheduleGame(game: Game)
    }
}