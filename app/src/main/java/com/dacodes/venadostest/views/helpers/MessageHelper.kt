package com.dacodes.venadostest.views.helpers

import android.content.DialogInterface
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.dacodes.venadostest.R
import com.google.android.material.snackbar.Snackbar

object MessageHelper {

    fun showDialog(
        activity: AppCompatActivity,
        title: String,
        message: String,
        callback: DialogInterface.OnClickListener
    ) {
        showDialogBuilder(activity, title, message, callback)
    }

    fun showDialog(activity: AppCompatActivity, title: String, message: String) {
        showDialogBuilder(activity, title, message, null)
    }

    private fun showDialogBuilder(
        activity: AppCompatActivity,
        title: String,
        message: String,
        callback: DialogInterface.OnClickListener?
    ) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(activity.getString(R.string.accept_text), callback)
            .create()
            .show()
    }

    fun show(view: View, message: String, fast: Boolean) {
        Snackbar.make(view, message, if (fast) Snackbar.LENGTH_SHORT else Snackbar.LENGTH_LONG).show()
    }
}
