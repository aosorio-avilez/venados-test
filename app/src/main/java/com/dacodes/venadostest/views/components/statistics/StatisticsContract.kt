package com.dacodes.venadostest.views.components.statistics

import com.dacodes.venadostest.data.models.Statistic
import com.dacodes.venadostest.views.base.BasePresenter
import com.dacodes.venadostest.views.base.BaseView
import com.dacodes.venadostest.views.base.Progressable

interface StatisticsContract {

    interface View : BaseView<Presenter>, Progressable {

        fun setupView()

        fun showStatistics(statistics: List<Statistic>)
    }

    interface Presenter : BasePresenter {

        fun requestStatistics()
    }
}