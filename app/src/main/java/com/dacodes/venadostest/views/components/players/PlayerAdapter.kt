package com.dacodes.venadostest.views.components.players

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Player

class PlayerAdapter(private val context: Context, private val listener: (Player) -> Unit) : BaseAdapter() {

    private var players: List<Player> = mutableListOf()

    override fun getCount(): Int {
        return players.size
    }

    override fun getItem(position: Int): Player {
        return players[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var currentView = convertView

        if (currentView == null) {
            currentView = LayoutInflater.from(context).inflate(R.layout.item_player, parent, false)
        }

        val pictureImageView = currentView?.findViewById(R.id.pictureImageView) as? ImageView
        val positionLabel = currentView?.findViewById(R.id.positionLabel) as? TextView
        val nameLabel = currentView?.findViewById(R.id.nameLabel) as? TextView

        val item = getItem(position)

        positionLabel?.text = item.position
        nameLabel?.text = String.format("%s %s", item.name, item.firstSurname)
        pictureImageView?.let {
            Glide.with(context)
                    .load(item.image)
                    .apply(RequestOptions.circleCropTransform())
                    .into(it)
        }
        currentView?.setOnClickListener { listener(item) }
        return currentView!!
    }

    fun setPlayers(players: List<Player>) {
        this.players = players
        notifyDataSetChanged()
    }
}