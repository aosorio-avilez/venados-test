package com.dacodes.venadostest.views.components.statistics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Statistic
import com.dacodes.venadostest.data.remote.services.StatisticsService
import kotlinx.android.synthetic.main.fragment_statistics.*
import kotlinx.android.synthetic.main.progress_bar_inline.*

class StatisticsFragment : Fragment(), StatisticsContract.View {

    override var presenter: StatisticsContract.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_statistics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = StatisticsPresenter(this, StatisticsService())

        presenter?.bind()
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun setupView() {
        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.adapter = StatisticAdapter()
    }

    override fun showStatistics(statistics: List<Statistic>) {
        (recyclerView.adapter as? StatisticAdapter)?.setstatistics(statistics)
    }

    override fun showProgress() {
        recyclerView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        recyclerView?.visibility = View.VISIBLE
    }
}
