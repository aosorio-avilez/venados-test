package com.dacodes.venadostest.views.components.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.dacodes.venadostest.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainContract.View {

    override var presenter: MainContract.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)

        presenter?.bind()
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun onBackPressed() {
        drawerLayout?.let {
            when {
                it.isDrawerOpen(GravityCompat.START) -> it.closeDrawer(GravityCompat.START)
                else -> super.onBackPressed()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        presenter?.onMenuOptionSelected(item.itemId)
        return true
    }

    override fun setupView() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
        }
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        drawerLayout?.addDrawerListener(toggle)
        toggle.syncState()
        navView?.setNavigationItemSelectedListener(this)
        navView?.setCheckedItem(R.id.nav_home)
        onNavigationItemSelected(navView.menu.getItem(0))
    }

    override fun showFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment, fragment.tag)
        fragmentTransaction.commit()
        drawerLayout?.closeDrawer(GravityCompat.START)
    }
}
