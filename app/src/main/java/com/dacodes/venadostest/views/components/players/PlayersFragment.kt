package com.dacodes.venadostest.views.components.players

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Player
import com.dacodes.venadostest.data.remote.services.PlayersService
import kotlinx.android.synthetic.main.fragment_players.*
import kotlinx.android.synthetic.main.progress_bar_inline.*

class PlayersFragment : Fragment(), PlayersContract.View {

    override var presenter: PlayersContract.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_players, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = PlayersPresenter(this, PlayersService())

        presenter?.bind()
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun setupView() {
        context?.apply {
            gridView?.adapter = PlayerAdapter(this) { player ->
                presenter?.onPlayerSelected(player)
            }
        }
        ViewCompat.setNestedScrollingEnabled(gridView, true)
    }

    override fun showPlayers(players: List<Player>) {
        (gridView?.adapter as? PlayerAdapter)?.setPlayers(players)
    }

    override fun showPlayerDetailDialog(player: Player) {
        context?.let { PlayerDialog(it, player).show() }
    }

    override fun showProgress() {
        gridView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        gridView?.visibility = View.VISIBLE
    }
}
