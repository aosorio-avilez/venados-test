package com.dacodes.venadostest.views.base

interface BaseView<T> {

    var presenter: T?
}