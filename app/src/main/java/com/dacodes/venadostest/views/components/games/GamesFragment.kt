package com.dacodes.venadostest.views.components.games

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dacodes.venadostest.R
import com.dacodes.venadostest.views.commons.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_games.*

class GamesFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_games, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureTabs()
    }

    private fun configureTabs() {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(GamesPerMonthFragment.newInstance(GamesPerMonthFragment.LeagueType.Ascent), getString(R.string.ascenso_mx_text))
        adapter.addFragment(GamesPerMonthFragment.newInstance(GamesPerMonthFragment.LeagueType.Cup), getString(R.string.copa_mx_text))
        viewPager?.adapter = adapter
        tabLayout?.setupWithViewPager(viewPager)
    }
}
