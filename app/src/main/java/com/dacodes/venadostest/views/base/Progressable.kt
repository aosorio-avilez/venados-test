package com.dacodes.venadostest.views.base

interface Progressable {
    fun showProgress()

    fun hideProgress()
}