package com.dacodes.venadostest.views.components.games

import com.dacodes.venadostest.data.models.Game
import com.dacodes.venadostest.data.remote.services.GameService

class GamesPerMonthPresenter(private var view: GamesPerMonthContract.View?,
                             private var leagueType: GamesPerMonthFragment.LeagueType?,
                             private var gamesService: GameService?) : GamesPerMonthContract.Presenter {

    override fun requestGames(withRefreshing: Boolean) {
        if (withRefreshing.not()) view?.showProgress()

        gamesService?.games({
            view?.hideProgress()
            val games = getGamesByLeague(it.games)
            view?.showEmptyView(games.isEmpty())
            setGames(games)
        }, { _, _ ->
            view?.hideProgress()
        })
    }

    override fun setGames(games: List<Game>) {
        val items: MutableMap<String, List<Game>> = mutableMapOf()
        val months = games.map { it.getMonthName() }.distinctBy { it }

        months.forEach { month ->
            if (games.any { it.getMonthName() == month }) {
                items[month] = games.filter { it.getMonthName() == month }
            }
        }
        view?.showGames(items)
    }

    override fun onScheduleGame(game: Game) {
        view?.scheduleGame(game)
    }

    override fun bind() {
        view?.setupView()
        requestGames()
    }

    override fun unbind() {
        leagueType = null
        gamesService = null
        view = null
    }

    private fun getGamesByLeague(games: List<Game>): List<Game> {
        return games.filter { game -> game.league == leagueType?.description }
    }
}