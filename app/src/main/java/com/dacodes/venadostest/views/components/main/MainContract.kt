package com.dacodes.venadostest.views.components.main

import androidx.fragment.app.Fragment
import com.dacodes.venadostest.views.base.BasePresenter
import com.dacodes.venadostest.views.base.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {

        fun setupView()

        fun showFragment(fragment: Fragment)
    }

    interface Presenter : BasePresenter {

        fun onMenuOptionSelected(menuId: Int)
    }
}