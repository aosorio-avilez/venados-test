package com.dacodes.venadostest.views.components.games

import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.CalendarContract.Events
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Game
import com.dacodes.venadostest.data.remote.services.GameService
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_game_per_month.*
import kotlinx.android.synthetic.main.progress_bar.*

class GamesPerMonthFragment : Fragment(), GamesPerMonthContract.View {

    enum class LeagueType(val description: String) {
        Cup("Copa MX"),
        Ascent("Ascenso MX")
    }

    override var presenter: GamesPerMonthContract.Presenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_game_per_month, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val leagueType = LeagueType.values()[arguments?.getInt(leagueTypeKey) ?: 0]

        presenter = GamesPerMonthPresenter(this, leagueType, GameService())

        presenter?.bind()
    }

    override fun onDestroy() {
        presenter?.unbind()
        super.onDestroy()
    }

    override fun setupView() {
        refreshLayout?.setOnRefreshListener { presenter?.requestGames(true) }
        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.adapter = SectionedRecyclerViewAdapter()
    }

    override fun showGames(gamesByMonth: Map<String, List<Game>>) {
        (recyclerView?.adapter as SectionedRecyclerViewAdapter).apply {
            removeAllSections()
            gamesByMonth.keys.forEach {
                addSection(GamePerMonthSection(it, gamesByMonth.getValue(it)) { game ->
                    presenter?.onScheduleGame(game)
                })
            }
            notifyDataSetChanged()
        }
    }

    override fun scheduleGame(game: Game) {
        val intent = Intent(Intent.ACTION_INSERT)
                .setData(Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, game.datetime.time)
                .putExtra(Events.TITLE, "${getString(R.string.vendados_name_text)} VS ${game.opponent}")
                .putExtra(Events.DESCRIPTION, game.league)
                .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
        startActivity(intent)
    }

    override fun showEmptyView(show: Boolean) {
        if (show) {
            emptyLabel?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        } else {
            emptyLabel?.visibility = View.GONE
            recyclerView?.visibility = View.VISIBLE
        }
    }

    override fun showProgress() {
        recyclerView?.visibility = View.GONE
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        recyclerView?.visibility = View.VISIBLE
        if (refreshLayout.isRefreshing) refreshLayout.isRefreshing = false
    }

    companion object {
        const val leagueTypeKey = "serviceRequestTypeExtra"

        fun newInstance(leagueType: LeagueType): GamesPerMonthFragment {
            val fragment = GamesPerMonthFragment()
            val args = Bundle()
            args.putInt(GamesPerMonthFragment.leagueTypeKey, leagueType.ordinal)
            fragment.arguments = args
            return fragment
        }
    }
}
