package com.dacodes.venadostest.views.components.games

import android.text.format.DateFormat
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dacodes.venadostest.R
import com.dacodes.venadostest.data.models.Game
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import kotlinx.android.synthetic.main.header_game.view.*
import kotlinx.android.synthetic.main.item_game.view.*

class GamePerMonthSection(private val month: String,
                          private val games: List<Game>, private val callback: (Game) -> Unit) :
        StatelessSection(
                SectionParameters.builder()
                        .itemResourceId(R.layout.item_game)
                        .headerResourceId(R.layout.header_game)
                        .build()
        ) {

    override fun getContentItemsTotal() = games.size

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        (holder as HeaderViewHolder).bind(month)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as GameViewHolder).bind(games[position], callback)
    }

    override fun getHeaderViewHolder(view: View?) = HeaderViewHolder(view!!)

    override fun getItemViewHolder(view: View?) = GameViewHolder(view!!)

    inner class GameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Game, listener: (Game) -> Unit) = with(itemView) {
            dayLabel?.text = DateFormat.format("dd", item.datetime)
            dayDescriptionLabel?.text = DateFormat.format("EEE", item.datetime)
            homeTeamNameLabel?.text = context.getString(R.string.vendados_name_text)
            awayTeamNameLabel?.text = item.opponent
            homeScoreLabel?.text = item.homeScore.toString()
            awayScoreLabel?.text = item.awayScore.toString()
            homeTeamImageView.setImageResource(R.mipmap.ic_venados_logo)
            Glide.with(context)
                    .load(item.opponentImage)
                    .into(awayTeamImageView)
            calendarImageView.setOnClickListener { listener(item) }
        }
    }

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(month: String) = with(itemView) {
            mountLabel?.text = month
        }
    }
}