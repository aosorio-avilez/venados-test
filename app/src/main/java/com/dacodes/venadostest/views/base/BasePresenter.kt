package com.dacodes.venadostest.views.base

interface BasePresenter {
    fun bind()

    fun unbind()
}