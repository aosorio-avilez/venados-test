# Venados Test

This is an example application of Venados F.C.

## About Venados Test

This app is a android test from DaCodes

## Getting Started

### Clone the Repository

To get started, cloning the project to your local machine:

```
$ https://bitbucket.org/aosorio-avilez/venados-test.git
```

### Open and Run Project in Android Studio

Now that you have cloned the repo:

1. Open Android Studio

2. Press "Open an existing Android Studio Project"

3. Select the folder where you cloned the repository

2. Press the run app button